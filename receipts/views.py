from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, create_category_form

# a view that will get all of the instances of the Receipt model and put them in the context for the template.


@login_required
def list_view_receipt(request):
    rec_gal = Receipt.objects.all().filter(purchaser=request.user)
    context = {
        "rec_gal_veiw": rec_gal,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = CreateReceipt()

    context = {"create_new": form}

    return render(request, "receipts/create.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    # request.user is who is currently logged in
    context = {"accounts": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/category_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = create_category_form(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = create_category_form()

    context = {"form": form}
    return render(request, "receipts/create_cat.html", context)
