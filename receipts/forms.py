from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class create_category_form(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]
