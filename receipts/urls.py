from django.urls import path
from receipts.views import (
    list_view_receipt,
    create_receipt,
    category_list,
    account_list,
    create_category,
)

urlpatterns = [
    path("", list_view_receipt, name="home"),
    path("create/", create_receipt, name=("create")),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="accounts_list"),
    path("categories/create/", create_category, name="create_category"),
]
